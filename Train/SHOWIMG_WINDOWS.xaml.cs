﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Train
{
    
    /// <summary>
    /// SHOWIMG_WINDOWS.xaml 的交互逻辑
    /// </summary>
    public partial class SHOWIMG_WINDOWS : Window
    {
        
        private bool isMouseLeftButtonDown = false;
        private Point previousMousePoint;

        public SHOWIMG_WINDOWS()
        {
            InitializeComponent();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            MainWindow.imgWindow = null;
        }

        //鼠标左键按下
        private void TestContentControl1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ContentControl rectangle = sender as ContentControl;
            if (rectangle == null)
                return;
            rectangle.CaptureMouse();
            isMouseLeftButtonDown = true;
            previousMousePoint = e.GetPosition(rectangle);
        }

        //鼠标左键松开
        private void TestContentControl1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ContentControl rectangle = sender as ContentControl;
            if (rectangle == null)
                return;
            rectangle.ReleaseMouseCapture();
            isMouseLeftButtonDown = false;
        }

        //鼠标移动
        private void TestContentControl1_MouseMove(object sender, MouseEventArgs e)
        {
            ContentControl image = sender as ContentControl;
            if (image == null)
            {
                return;
            }
            if (isMouseLeftButtonDown)
            {
                DoImageMove(image, e.GetPosition(image));
            }
        }

        //移动TransformGroup，image位置绑定TransformGroup
        private void DoImageMove(ContentControl image, Point position)
        {
            TransformGroup group = ImageComparePanel.FindResource("ImageCompareResources") as TransformGroup;
            Debug.Assert(group != null, "Can't find transform group from image compare panel resource");
            TranslateTransform transform = group.Children[1] as TranslateTransform;
            transform.X += position.X - previousMousePoint.X;
            transform.Y += position.Y - previousMousePoint.Y;
            previousMousePoint = position;
        }

        //滚轮
        private void MasterImage_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            ContentControl image = sender as ContentControl;
            if (image == null)
            {
                return;
            }
            TransformGroup group = ImageComparePanel.FindResource("ImageCompareResources") as TransformGroup;
            Debug.Assert(group != null, "Can't find transform group from image compare panel resource");
            Point point = e.GetPosition(image);
            double scale = e.Delta * 0.001;
            ZoomImage(group, point, scale);
        }

        //放大缩小
        private static void ZoomImage(TransformGroup group, Point point, double scale)
        {
            Debug.Assert(group != null, "Oops, ImageCompareResources is removed from current control's resouce");
            Point pointToContent = group.Inverse.Transform(point);
            ScaleTransform transform = group.Children[0] as ScaleTransform;
            if (transform.ScaleX + scale < 1)
            {
                return;
            }
            transform.ScaleX += scale;
            transform.ScaleY += scale;
            TranslateTransform transform1 = group.Children[1] as TranslateTransform;
            transform1.X = -1 * ((pointToContent.X * transform.ScaleX) - point.X);
            transform1.Y = -1 * ((pointToContent.Y * transform.ScaleY) - point.Y);
        }
    }
}
